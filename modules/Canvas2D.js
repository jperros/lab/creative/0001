import Stats from 'https://cdnjs.cloudflare.com/ajax/libs/stats.js/r17/Stats.min.js'

export default class Canvas2D {
   /**
    * Initialize a 2D drawing context
    * 
    * @param {Object} options
    * @param {String} options.el
    * @param {Number} [options.fps=60]
    * @param {Object} [options.on]
    * @param {Function} [options.on.mousemove]
    * @param {Function} [options.on.resize]
    * @param {Function} [options.on.draw]
    */
   constructor(options) {
      this.el = document.querySelector(options.el);
      this.fps = options.fps || 60;
      this.context = this.el.getContext('2d');

      this.time = {
         interval: 1000 / this.fps,
         start: Date.now(),
         now: null,
         then: Date.now(),
         elapsed: null
      };

      this.on = {
         mousemove: options?.on?.mousemove,
         resize: options?.on?.resize,
         draw: options?.on?.draw
      };

      this._handler = {
         resize: this.handleResize.bind(this),
         mousemove: this.handleMouseMove.bind(this),
         draw: this.draw.bind(this),
      };

      this.init()
   }

   init() {
      this.initStats();
      this.computeSize();
      this.bindEvents();

      this._raf = requestAnimationFrame(this._handler.draw);
   }

   bindEvents() {
      window.addEventListener('mousemove', this._handler.mousemove);
      window.addEventListener('touchmove', this._handler.mousemove);
      window.addEventListener('resize', this._handler.resize);
   }

   initStats() {
      this.stats = new Stats();
      this.stats.showPanel(0);

      document.body.insertAdjacentElement('afterbegin', this.stats.dom);
   }

   computeSize() {
      let width = window.innerWidth;
      let height = window.innerHeight;
      let pixelRatio = window.devicePixelRatio;
      
      this.width = width;
      this.height = height;

      this.el.width = width * pixelRatio;
      this.el.height = height * pixelRatio;

      this.el.style.width = `${width}px`;
      this.el.style.height = `${height}px`;

      this.context.scale(pixelRatio, pixelRatio);
   }

   handleMouseMove(e) {
      if (window.TouchEvent && e instanceof TouchEvent) {
         e = e.touches[0];
      }

      this.on.mousemove?.(e);
   }

   handleResize(e) {
      this.computeSize();
      this.on.resize?.(e);
   }

   draw() {
      this.time.now = Date.now();
      this.time.elapsed = this.time.now - this.time.then;

      if (this.time.elapsed > this.time.interval) {
         this.stats.begin();

         this.time.then = this.time.now - (this.time.elapsed % this.time.interval);
         this.on.draw?.(this.context);

         this.stats.end();
      }

      this._raf = requestAnimationFrame(this._handler.draw);
   }

   destroy() {
      window.removeEventListener('mousemove', this._handler.mousemove);
      window.removeEventListener('touchmove', this._handler.mousemove);
      window.removeEventListener('resize', this._handler.resize);

      cancelAnimationFrame(this._raf);

      this._handler = null;
      this._raf = null;

      this.el = null;
      this.fps = null;
      this.context = null;
      this.time = null;
      this.on = null;
   }
}