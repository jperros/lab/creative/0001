import { IS_DARK_MODE_ENABLED, lerp } from "./modules/Utils.js";
import Canvas2D from "./modules/Canvas2D.js";

const config = {
   gap: 24,
   size: 1,
   padding: 20,
   color: IS_DARK_MODE_ENABLED ? '#FFFFFF' : '#0A0A0A'
}

const canvas = new Canvas2D({
   el: '#canvas',
   fps: 60,
   on: {
      mousemove: handleMouseMove,
      resize: computeDotsPositions,
      draw: handleDraw
   }
});

const dots = [];

const cursor = {
   x: canvas.width * 0.5,
   y: canvas.height * 0.5,
   size: 1,
   targetX: canvas.width * 0.5,
   targetY: canvas.height * 0.5,
   targetSize: 100
}

computeDotsPositions();


function computeDotsPositions() {
   let availableWidth = canvas.width - config.padding * 2
   let availableHeight = canvas.height - config.padding * 2

   let rowCount = Math.floor(availableHeight / (config.size + config.gap))
   let columnCount = Math.floor(availableWidth / (config.size + config.gap))

   let filledWidth = columnCount * (config.size + config.gap) - config.gap
   let filledHeight = rowCount * (config.size + config.gap) - config.gap

   let horizontalWhiteSpace = availableWidth - filledWidth
   let verticalWhiteSpace = availableHeight - filledHeight

   if (dots.length) {
      dots.splice(0, dots.length);
   }

   for (let row = 0; row < rowCount; row++) {
      for (let column = 0; column < columnCount; column++) {
         let x = column * (config.size + config.gap) + config.padding + (horizontalWhiteSpace * 0.5)
         let y = row * (config.size + config.gap) + config.padding + (verticalWhiteSpace * 0.5)

         dots.push({
            x,
            y,
            size: config.size,
            _x: x,
            _y: y,
            _size: config.size
         })
      }
   }
}


function handleMouseMove(e) {
   cursor.targetX = e.clientX;
   cursor.targetY = e.clientY;
}


function handleDraw(ctx) {
   if (
      Math.abs(cursor.x - cursor.targetX) > 0.01 ||
      Math.abs(cursor.y - cursor.targetY) > 0.01 ||
      Math.abs(cursor.size - cursor.targetSize) > 0.01
   ) {
      ctx.fillStyle = config.color;

      cursor.x = lerp(cursor.x, cursor.targetX, 0.08);
      cursor.y = lerp(cursor.y, cursor.targetY, 0.08);

      if (cursor.size < 100) {
         cursor.size = lerp(cursor.size, cursor.targetSize, 0.15);
      } else {
         cursor.size = lerp(cursor.size, cursor.targetSize, 0.1);
      }

      ctx.clearRect(0, 0, canvas.width, canvas.height);

      let cursorDistanceX = cursor.x - cursor.targetX;
      let cursorDistanceY = cursor.y - cursor.targetY;
      let cursorDistance = Math.sqrt(cursorDistanceX * cursorDistanceX + cursorDistanceY * cursorDistanceY);

      let cursorScaleFactor = 2 - Math.min(1, (window.innerHeight - cursorDistance) / window.innerHeight);
      cursor.targetSize = 100 * cursorScaleFactor;

      dots.map(dot => {
         let distanceX = dot._x - cursor.x;
         let distanceY = dot._y - cursor.y;
         let distance = Math.sqrt(distanceX * distanceX + distanceY * distanceY);

         // Check if point is in "cursor" radius
         if (distance <= cursor.size) {

            let dist = (cursor.size - distance) / cursor.size;            
            
            let x = (dot._x - cursor.x) * (1 + dist) + cursor.x;
            let y = (dot._y - cursor.y) * (1 + dist) + cursor.y;
            let size = Math.max(dist * 24, config.size);

            ctx.beginPath();
            ctx.arc(x, y, size * 0.5, Math.PI * 2, false);
            ctx.closePath();
            ctx.fill();

            dot.x = x;
            dot.y = y;
            dot.size = size;
         } else {
            ctx.beginPath();
            ctx.arc(dot._x, dot._y, config.size * 0.5, Math.PI * 2, false);
            ctx.closePath();
            ctx.fill();
         }
      });
   }
}