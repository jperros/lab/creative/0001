export const IS_DARK_MODE_ENABLED = window.matchMedia('(prefers-color-scheme: dark)').matches;

export const lerp = (from, to, amount = 0.1) => (1 - amount) * from + amount * to;